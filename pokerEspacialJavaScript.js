//VARIAVEL PARA CHECAR VALORES DAS CARTAS:
var card_value = {
    "1":10, //NÃO TEM UM NO BARALHO, LOGO
    "2":2,
    "3":3,
    "4":4,
    "5":5,
    "6":6,
    "7":7,
    "8":8,
    "9":9,
    "J":11,
    "Q":12,
    "K":13,
    "A":14
}

var combo_scores = {
    "Royal Flush": 100,
    "Straight Flush": 90,
    "Quadra": 80,
    "Full House": 70,
    "Flush": 60,
    "Straight": 50,
    "Trio": 40,
    "Dois Pares": 30,
    "Par": 20,
    "Carta Alta": 0
}



//FUNÇÃO PARA GERAR INTEIRO ALEATORIO BASEADO EM INTERVALO
function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


//FUNÇÃO PARA COLOCAR AS CARTAS EM UMA SEQUENCIA CRESCENTE
function sortCards(deck) {
    for (var i = 0; i < deck.length; i++){
        for (var j = i+1; j < deck.length; j++){

            if (card_value[deck[j][0]] < card_value[deck[i][0]]){
                var aux = deck[i];
                deck[i] = deck[j];
                deck[j] = aux;
            }
        }
    }
    
    return deck;
}



//COMO AS CARTÃOS IRÃO ESTAR ORDENADAS EM ORDEM CRESCENTE, É CRIADO ESSA FUNÇÃO PARA VERIFICAR AS COMBINAÇÕES:
function checkCombinations(work_deck, origin_deck){
    //WORK_DECK SERIA A MÃO DO JOGADOR + DECK DA MESA PARA VERIFICAR COMBINAÇÕES
    //ORIGIN_DECK EQUIVALE A PROPRIA MÃO DO JOGADOR, QUE VAI SER USADO PARA CONFERIR SE ALGUMA DAS CARTAS DO JOGADOR ESTÃO CONTIDAS NAS POSSIVEIS COMBINAÇÕES A SEREM FEITAS
    
    
    //SÓ EXISTEM 4 FORMAS DE TER UM ROYAL FLUSH, ENTÃO VERIFICAR ESSA COMBINAÇÃO É MAIS FÁCIL USANDO A FUNÇÃO "INCLUDES"
    var royal_flushes = ["10C JC QC KC AC", "10E JE QE KE AE", "10O JO QO KO AO", "10P JP QP KP AP"];

    //INICIALIZAR O ARRAY DE COMBINAÇÕES E SUAS CARTAS
    var combos = [];

    //PEGAR AS PRIMEIRAS DUAS CARTAS PARA COMPARAR COM AS CARTAS SEGUINTES, JÁ QUE ESTÃO ORDENADAS, DA PRA CHECAR COMBINAÇÕES
    for (var i = 0; i < 3; i++){
        var first_card = work_deck[i];
        var second_card = work_deck[i+1];
        var third_card = work_deck[i+2];
        var fourth_card = work_deck[i+3];
        var last_card = work_deck[i+4];

        var return_hand = ["score", first_card, second_card, third_card, fourth_card, last_card];

        var check_royals =  first_card + " " + second_card + " " + third_card + " " + fourth_card + " " + last_card; 

        if (royal_flushes.includes(check_royals)){ 
            return_hand[0] = "Royal Flush";
            combos.push(return_hand);
        } else if (card_value[first_card[0]] + 1 == card_value[second_card[0]] && card_value[second_card[0]] + 1 == card_value[third_card[0]] && card_value[third_card[0]] + 1 == card_value[fourth_card[0]] && card_value[fourth_card[0]] + 1 == card_value[last_card[0]] && first_card.slice(-1) == second_card.slice(-1) && second_card.slice(-1) == third_card.slice(-1) && third_card.slice(-1) == fourth_card.slice(-1) && fourth_card.slice(-1) == last_card.slice(-1) ){
            return_hand[0] = "Straight Flush";
            combos.push(return_hand);
        } else if ( ( first_card[0] == second_card[0] && second_card[0] == third_card[0] && third_card[0] == fourth_card[0] ) || ( second_card[0] == third_card[0] && third_card[0] == fourth_card[0] && fourth_card[0] == last_card[0] )){
            return_hand[0] = "Quadra";
            combos.push(return_hand);
        } else if ( (first_card[0] == second_card[0] && second_card[0] == third_card[0] && fourth_card[0] == last_card[0] ) || ( first_card[0] == second_card[0] && third_card[0] == fourth_card[0] && fourth_card[0] == last_card[0] )){
            return_hand[0] = "Full House";
            combos.push(return_hand);
        } else if (first_card.slice(-1) == second_card.slice(-1) && second_card.slice(-1) == third_card.slice(-1) && third_card.slice(-1) == fourth_card.slice(-1) && fourth_card.slice(-1) == last_card.slice(-1)){
            return_hand[0] = "Flush";
            combos.push(return_hand);
        } else if ( card_value[first_card[0]] + 1 == card_value[second_card[0]] && card_value[second_card[0]] + 1 == card_value[third_card[0]] && card_value[third_card[0]] + 1 == card_value[fourth_card[0]] && card_value[fourth_card[0]] + 1 == card_value[last_card[0]] ){
            return_hand[0] = "Straight";
            combos.push(return_hand);
        } else if ( (first_card[0] == second_card[0] && second_card[0] == third_card[0] ) || (second_card[0] == third_card[0] && third_card[0] == fourth_card[0]) || (third_card[0] == fourth_card[0] && fourth_card[0] == last_card[0])){
            return_hand[0] = "Trio";
            combos.push(return_hand);
        } else if ( ( first_card[0] == second_card[0] && third_card[0] == fourth_card[0] ) || ( first_card[0] == second_card[0] && fourth_card[0] == last_card[0] ) || (second_card[0] == third_card[0] && fourth_card[0] == last_card[0])) {
            return_hand[0] = "Dois Pares";
            combos.push(return_hand);
        } else if ( first_card[0] == second_card[0] || second_card[0] == third_card[0] || third_card[0] == fourth_card[0] || fourth_card[0] == last_card[0]){
            return_hand[0] = "Par";
            combos.push(return_hand);
        } else{
            return_hand[0] = "Carta Alta";
            combos.push(return_hand);
        }    
    }

    //ARRAY PARA ARMAZENAR OS COMBOS VÁLIDOS (AQUELES QUE POSSUEM PELO MENOS UMA DAS CARTAS DO JOGADOR)
    var valid_combos = [];
    var first_check_card = origin_deck[0];
    var second_check_card = origin_deck[1];

    //PERCORRER O ARRAY DE COMBOS PARA VERIFICAR EXISTENCIA DAS CARTAS DO JOGADOR NAS COMBINAÇÕES
    for (let i = 0; i < combos.length; i++) {
        var check = combos[i];
        if (check.includes(first_check_card) || check.includes(second_check_card)){
            valid_combos.push(check);
        }
    }

    //FAZENDO AJUSTES PARA COMBINAÇÃO "CARTA ALTA", AS VEZES A CARTA DO JOGADOR ESTÁ NA COMBINAÇÃO, MAS O ALGORITMO NÃO USA O VALOR DAS CARTAS DO JOGADOR PARA O VALOR DA CARTA ALTA, POIS PODE HAVER UMA CARTA DE VALOR MAIOR NA MESA
    

    //SETANDO O PRIMEIRO COMBO VALIDO COMO MAIOR SCORE, PARA COMPARAR COM OS OUTROS:
    var first_combo = valid_combos[0];
    var top_combo = {
        "score": combo_scores[first_combo[0]],
        "deck":first_combo
    }

    //ATUALIZANDO O VALOR DO SCORE, CASO SEJA "CARTA ALTA" E TRATANDO EXCEÇÃO DA CARTA ALTA
    //EXCEÇÃO: AS VEZES A CARTA DO JOGADOR ESTÁ PRESENTE NA COMBINAÇÃO "CARTA ALTA", MAS O ALGORITMO NÃO USA AS CARTAS DO JOGADOR PARA ESCOLHER A CARTA ALTA, MAS SIM UMA CARTA ALTA DA MESA
    
    //COLOCAR AS CARTAS DA MÃO EM SEQUENCIA, PARA AI ESCOLHER A CARTA MAIS ALTA E A MENOR
    var sorted_origin_deck = sortCards(origin_deck);
    var higher_card = sorted_origin_deck[1]; 
    var another_card = sorted_origin_deck[0];
    
    //CASO SEJA "CARTA ALTA", REALIZAR AS ALTERAÇÕES
    if (top_combo["score"] == 0){
        
        if (top_combo['deck'].includes(higher_card)){ //SE TIVER A CARTA MAIS ALTA DA MÃO DO JOGADOR, USE-A COMO PARAMETRO DE PONTUAÇÃO
            var pick_card = higher_card;
            var new_score = card_value[pick_card[0]];
            top_combo['score'] = new_score;
        } else if (top_combo['deck'].includes(another_card)){ //CASO NÃO TENHO A MAIS ALTA, PEGUE A MENOR E USE A COMO PARAMETRO DE PONTO
            var pick_card = another_card;
            var new_score = card_value[pick_card[0]];
            top_combo['score'] = new_score;
        } else { //CASO NÃO HAJA NENHUMA DAS DUAS, MAS O RESULTADO FOI CARTA ALTA, O RESULTADO NÃO É VALIDO, POIS A CARTA MAIS ALTA É DA MESA
            top_combo['score'] = 0;
        }
        
        
    }
    

    //PERCORRENDO O VETOR PARA REALIZAR AS COMPARAÇÕES
    for (let i = 0; i < valid_combos.length; i++) {
        var element = valid_combos[i];
        var element_score = combo_scores[element[0]];

        //ATUALIZANDO O VALOR DO ELEMENT_SCORE, CASO SEJA "CARTA ALTA", USANDO A MESMA LÓGICA ANTERIOR
        if (element_score == 0){
            if (element.includes(higher_card)){
                var pick_element_card = higher_card;
                var element_score = card_value[pick_element_card[0]];
            } else if (element.includes(another_card)){
                var pick_element_card = another_card;
                var element_score = card_value[pick_element_card[0]];
            } else {
                element_score = 0;
            }
            
                
        //REALIZANDO A MUDANÇA DE SCORE E DE COMBINAÇÃO
        if (element_score > top_combo.score){
            top_combo.deck = element;
            top_combo.score = element_score;
        }
        
        }

    
    }
    
    return top_combo;
    
}

var naipes = ["C", "E", "O", "P"];
var cartas = ["2", "3", '4', '5', '6', '7', '8', '9', '10','A', 'J', 'Q', 'K'];

//FORMAR DECK
var deck = [];
for (var i = 0; i < naipes.length; i++){
    for (var j = 0; j < cartas.length; j++){
        deck.push(cartas[j] + naipes[i]);
    }
}

//DISTRIBUIR CARTAS DE FORMA ALEATORIA (MESA ; ROBO_1; ROBO_2)
var deck_um = [];
var deck_dois = [];
var deck_mesa = [];

//DISTRIBUIR CARTAS AOS ROBOS
for(var i = 0; i < 2; i++){
    var index_um = randomInteger(0,deck.length-1);
    var carta_um = deck.splice(index_um, 1);
    deck_um.push(carta_um[0]);
    
    
    var index_dois = randomInteger(0, deck.length-1);
    var carta_dois = deck.splice(index_dois, 1);
    deck_dois.push(carta_dois[0]);
    
    
}

//DISTRIBUIR CARTAS A MESA:
for (var i = 0; i < 5; i++){
    var index_mesa = randomInteger(0,deck.length-1);
    var carta_mesa = deck.splice(index_mesa, 1);
    deck_mesa.push(carta_mesa[0]);
}



//CRIAÇÃO DO DECK DE COMBINAÇÕES DE CADA JOGADOR
var work_deck_um = deck_um.concat(deck_mesa);
var work_deck_dois = deck_dois.concat(deck_mesa);

//COLOCANDO AS CARTAS EM SEQUENCIA PARA CADA JOGADOR
sortCards(work_deck_um);
sortCards(work_deck_dois);

//TESTANDO COMBINAÇÕES ENVOLVENDO AS CARTAS SEQUENCIADAS, RETORNADO A MELHOR SEQUENCIA, SEU "SCORE" E SUA COMBINAÇÃO
var resultado_um = checkCombinations(work_deck_um, deck_um);
var resultado_dois = checkCombinations(work_deck_dois, deck_dois);

//MOSTRANDO A MELHOR COMBINAÇÃO DE CADA JOGADOR E SUAS SELEÇÃO DE CARTAS 
console.log("Robô 1: " + resultado_um['deck']);
console.log("Robô 2: " + resultado_dois["deck"]);


//HORA DE ACERTAR AS CONTAS! (OU OS SCORES, COMO PREFERIR)
if(resultado_um['score'] > resultado_dois['score']){
    console.log("Robô 1 venceu!");
} else if(resultado_dois['score'] > resultado_um['score']){
    console.log("Robô 2 venceu!");
} else if(resultado_um['score'] == resultado_dois['score']){
    console.log("Empate!");
}